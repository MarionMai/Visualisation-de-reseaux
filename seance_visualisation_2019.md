Contenu de la séance
====================

-   Principes de sémiologie graphique
-   Comment produire une représentation de graphe lisible et utile
    -   L’exemple de la distance entre langues européennes
    -   L’exemple du commerce mondial de métaux
        -   Présentation
        -   [Application sur le logiciel
            R](#application-sur-le-logiciel-R)
-   [Références](#références)

Application sur le logiciel R
=============================

Charger les packages *igraph* et *cartography*
----------------------------------------------

Ce tutoriel utilise plusieurs packages clés qu’il faudra installer.
D’autres seront mentionnés mais ne sont pas indispensables.

Les principaux packages qui seront utilisés sont
[*`igraph`*](https://cran.r-project.org/web/packages/igraph/index.html)
(développé par Gabor Csardi et Tamas Nepusz), et
[*`cartography`*](https://cran.r-project.org/web/packages/cartography/index.html)
(développé par Timothée Giraud et Nicolas Lambert).  

    library("igraph")
    library("cartography")

Charger les données
-------------------

Ouvrir une liste de liens

Le jeu de données comprend deux fichiers “vertices.csv” et
“edgelist.csv” (téléchargeables
[*ici*](http://vlado.fmf.uni-lj.si/pub/networks/data/esna/metalWT.htm)
au format Pajek et
[*ici*](https://framagit.org/MarionMai/Visualisation-de-reseaux/blob/master/vertices.csv)
et
[*ici*](https://framagit.org/MarionMai/Visualisation-de-reseaux/blob/master/edgelist.csv)
dans un format plus adapté pour l’importation sous R).  

    # ouvrir le fichier contenant la liste de liens
    e<-read.csv(file="edgelist.csv", header=T, sep=";")

    # ouvrir le fichier d'attribut des sommets
    av<-read.csv(file="vertices.csv", header=T, sep=";")

    # transformer la matrice en objet igraph
    net <- graph_from_data_frame(d=e, directed=T, vertices=av) 

Paramètres du graphe
--------------------

Les paramètres du graphe créé s’affichent. Ce graphe est “DNW-”, D pour
“*Directed*”, N pour“*Named*” indique que les sommets ont un nom et W
pour “*Weighted*”, ce qui veut dire que ce graphe est dirigé, unipartite
et valué.

80 correspond au nombre de sommets et 1000 au nombre de liens.

Les attributs sont le nom des sommets (noms de pays), leur appartenance
à des sous-ensembles (leur continent, leur position dans le système
centre-périphérie en 1980 et 1994), ainsi que leur PIB par habitant en
milliers de dollars américains en 1994. On dispose aussi de deux
systèmes de coordonnées (à tester).

La valeur des liens correspond à la valeur échangée en dollars
américains (1,000 US$) lors des importations de métaux sachant que dans
les pays où les importations de métaux représentaient moins d’1 % de la
valeur totale des importations nationales, elles n’ont pas été incluses.

    # consulter les paramètres du graphe
    net

    ## IGRAPH c386219 DNW- 80 1000 -- 
    ## + attr: name (v/c), Country (v/c), coordx (v/n), coordy (v/n),
    ## | wdsystem1980 (v/n), wdsystem1994 (v/n), continent (v/n), xcoord2
    ## | (v/n), ycoord2 (v/n), GDP1995 (v/n), weight (e/n)
    ## + edges from c386219 (vertex names):
    ##  [1] 78->25 24->25 26->25 38->25 78->28 7 ->28 24->28 26->28 38->28 78->53
    ## [11] 39->53 13->53 7 ->53 18->53 24->53 26->53 38->53 50->53 77->53 4 ->53
    ## [21] 23->53 71->53 50->53 78->24 7 ->24 18->24 26->24 38->24 50->24 69->24
    ## [31] 77->24 4 ->24 71->24 50->24 11->12 78->12 2 ->12 10->12 14->12 80->12
    ## [41] 39->12 13->12 41->12 24->12 26->12 38->12 69->12 77->12 71->12 72->12
    ## [51] 3 ->12 78->31 39->31 13->31 41->31 66->31 26->31 38->31 77->31 78->23
    ## + ... omitted several edges

    # creer un attribut label
    V(net)$label<-V(net)$Country

On constate que tous les attributs des sommets ont été intégrés dans
l’objet `igraph`. Voyons ce que donne une représentation de ce graphe
par défaut en utilisant la fonction `plot`.  

    # représenter le graphe par défaut
    plot(net)

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-5-1.png" style="display: block; margin: auto;" />

Le résultat est très brouillon. On repère des anomalies : Réunion,
Martinique, Guyane française, Guadeloupe. Ce sont des territoires
français qui auraient du être aggrégés au reste de la France. Pour
repérer tous les autres cas de ce genre, nous devons afficher la liste
des sommets. Ensuite, nous réduirons le graphe en re-aggrégeant les
données.

Aggréger des sommets
--------------------

    # afficher la liste des sommets
    V(net)$label

    ##  [1] "Algeria"          "Argentina"        "Australia"       
    ##  [4] "Austria"          "Barbados"         "Bangladesh"      
    ##  [7] "Belgium /Lux."    "Belize"           "Bolivia"         
    ## [10] "Brazil"           "Canada"           "Chile"           
    ## [13] "China"            "Colombia"         "Croatia"         
    ## [16] "Cyprus"           "Czech Rep."       "Denmark"         
    ## [19] "Ecuador"          "Egypt"            "El Salvador"     
    ## [22] "Fiji"             "Finland"          "France Mon."     
    ## [25] "French Guiana"    "Germany"          "Greece"          
    ## [28] "Guadeloupe"       "Guatemala"        "Honduras"        
    ## [31] "Hong Kong"        "Hungary"          "Iceland"         
    ## [34] "India"            "Indonesia"        "Ireland"         
    ## [37] "Israel"           "Italy"            "Japan"           
    ## [40] "Jordan"           "Korea. Rep. Of"   "Kuwait"          
    ## [43] "Latvia"           "Madagascar"       "Malaysia"        
    ## [46] "Martinique"       "Mauritius"        "Mexico"          
    ## [49] "Morocco"          "Netherlands"      "New Zealand"     
    ## [52] "Nicaragua"        "Norway"           "Oman"            
    ## [55] "Pakistan"         "Panama"           "Paraguay"        
    ## [58] "Peru"             "Philippines"      "Poland"          
    ## [61] "Portugal"         "Moldava. Rep. Of" "Reunion"         
    ## [64] "Romania"          "Seychelles"       "Singapore"       
    ## [67] "Slovenia"         "Southern Africa"  "Spain"           
    ## [70] "Sri Lanka"        "Sweden"           "Switzerland"     
    ## [73] "Thailand"         "Trinidad Tobago"  "Tunisia"         
    ## [76] "Turkey"           "United Kingdom"   "United States"   
    ## [79] "Uruguay"          "Venezuela"

En dehors des DOM (Départements d’Outre-Mer) français, il n’y a aucun
pays qui ne soit pas souverain dans cette liste. L’exception est Hong
Kong. L’île n’a pas encore été rattachée à la Chine en 1995 et est
encore sous influence britannique. Compte tenu de l’originalité de son
statut à cette période, nous décidons de lui laisser son indépendance.  

    ## rattacher les DOM au reste de la France en modifiant la liste des sommets

    # 1ere etape : repérer les identifiants des sommets concernés
    i<-which(V(net)$label %in% "France Mon.")
    j<-which(V(net)$label %in% "French Guiana")
    k<-which(V(net)$label %in% "Guadeloupe")
    l<-which(V(net)$label %in% "Martinique")
    m<-which(V(net)$label %in% "Reunion")

    # compter le nombre de sommets
    N<-length(V(net))

    # lister les sommets qui devront être aggrégés en un seul
    c(i,j,k,l,m)

    ## [1] 24 25 28 46 63

On obtient la liste des identifiants numériques des sommets à aggréger.
Pour aggréger les sommets d’un graphe, il faut utiliser la fonction
`contract.vertices`. L’argument nécessaire à cette fonction est
l’argument `mapping` permettant d’établir un nouveau vecteur
d’identifiants numériques qui remplacera le précédent. Les sommets
autrefois codés 25, 28, 46, et 63 seront aggrégés avec le sommet codé 24
qui conservera son identifiant.  

    # définir le vecteur permettant de réduire le graphe - 2 méthodes

    map<-c(1:(j-1),i,j:(k-2), i, (k-1):(l-3), i, (l-2):(m-4), i, (m-3):(N-4))

    map<-c(1:24,24,25:26,24,27:43,24,44:59,24,60:76)

    # consulter ce vecteur : l'identifiant 24 remplace les identifiants 25, 28, 46, 63
    cbind(map,V(net)$label)

    ##       map                    
    ##  [1,] "1"  "Algeria"         
    ##  [2,] "2"  "Argentina"       
    ##  [3,] "3"  "Australia"       
    ##  [4,] "4"  "Austria"         
    ##  [5,] "5"  "Barbados"        
    ##  [6,] "6"  "Bangladesh"      
    ##  [7,] "7"  "Belgium /Lux."   
    ##  [8,] "8"  "Belize"          
    ##  [9,] "9"  "Bolivia"         
    ## [10,] "10" "Brazil"          
    ## [11,] "11" "Canada"          
    ## [12,] "12" "Chile"           
    ## [13,] "13" "China"           
    ## [14,] "14" "Colombia"        
    ## [15,] "15" "Croatia"         
    ## [16,] "16" "Cyprus"          
    ## [17,] "17" "Czech Rep."      
    ## [18,] "18" "Denmark"         
    ## [19,] "19" "Ecuador"         
    ## [20,] "20" "Egypt"           
    ## [21,] "21" "El Salvador"     
    ## [22,] "22" "Fiji"            
    ## [23,] "23" "Finland"         
    ## [24,] "24" "France Mon."     
    ## [25,] "24" "French Guiana"   
    ## [26,] "25" "Germany"         
    ## [27,] "26" "Greece"          
    ## [28,] "24" "Guadeloupe"      
    ## [29,] "27" "Guatemala"       
    ## [30,] "28" "Honduras"        
    ## [31,] "29" "Hong Kong"       
    ## [32,] "30" "Hungary"         
    ## [33,] "31" "Iceland"         
    ## [34,] "32" "India"           
    ## [35,] "33" "Indonesia"       
    ## [36,] "34" "Ireland"         
    ## [37,] "35" "Israel"          
    ## [38,] "36" "Italy"           
    ## [39,] "37" "Japan"           
    ## [40,] "38" "Jordan"          
    ## [41,] "39" "Korea. Rep. Of"  
    ## [42,] "40" "Kuwait"          
    ## [43,] "41" "Latvia"          
    ## [44,] "42" "Madagascar"      
    ## [45,] "43" "Malaysia"        
    ## [46,] "24" "Martinique"      
    ## [47,] "44" "Mauritius"       
    ## [48,] "45" "Mexico"          
    ## [49,] "46" "Morocco"         
    ## [50,] "47" "Netherlands"     
    ## [51,] "48" "New Zealand"     
    ## [52,] "49" "Nicaragua"       
    ## [53,] "50" "Norway"          
    ## [54,] "51" "Oman"            
    ## [55,] "52" "Pakistan"        
    ## [56,] "53" "Panama"          
    ## [57,] "54" "Paraguay"        
    ## [58,] "55" "Peru"            
    ## [59,] "56" "Philippines"     
    ## [60,] "57" "Poland"          
    ## [61,] "58" "Portugal"        
    ## [62,] "59" "Moldava. Rep. Of"
    ## [63,] "24" "Reunion"         
    ## [64,] "60" "Romania"         
    ## [65,] "61" "Seychelles"      
    ## [66,] "62" "Singapore"       
    ## [67,] "63" "Slovenia"        
    ## [68,] "64" "Southern Africa" 
    ## [69,] "65" "Spain"           
    ## [70,] "66" "Sri Lanka"       
    ## [71,] "67" "Sweden"          
    ## [72,] "68" "Switzerland"     
    ## [73,] "69" "Thailand"        
    ## [74,] "70" "Trinidad Tobago" 
    ## [75,] "71" "Tunisia"         
    ## [76,] "72" "Turkey"          
    ## [77,] "73" "United Kingdom"  
    ## [78,] "74" "United States"   
    ## [79,] "75" "Uruguay"         
    ## [80,] "76" "Venezuela"

    # avec vertex.attr.comb, dire comment les attributs des sommets aggrégés seront combinés
    g<-contract(net,mapping= map , vertex.attr.comb=list("first"))

    # consulter la nouvelle liste des sommets
    V(g)$label

    ##  [1] "Algeria"          "Argentina"        "Australia"       
    ##  [4] "Austria"          "Barbados"         "Bangladesh"      
    ##  [7] "Belgium /Lux."    "Belize"           "Bolivia"         
    ## [10] "Brazil"           "Canada"           "Chile"           
    ## [13] "China"            "Colombia"         "Croatia"         
    ## [16] "Cyprus"           "Czech Rep."       "Denmark"         
    ## [19] "Ecuador"          "Egypt"            "El Salvador"     
    ## [22] "Fiji"             "Finland"          "France Mon."     
    ## [25] "Germany"          "Greece"           "Guatemala"       
    ## [28] "Honduras"         "Hong Kong"        "Hungary"         
    ## [31] "Iceland"          "India"            "Indonesia"       
    ## [34] "Ireland"          "Israel"           "Italy"           
    ## [37] "Japan"            "Jordan"           "Korea. Rep. Of"  
    ## [40] "Kuwait"           "Latvia"           "Madagascar"      
    ## [43] "Malaysia"         "Mauritius"        "Mexico"          
    ## [46] "Morocco"          "Netherlands"      "New Zealand"     
    ## [49] "Nicaragua"        "Norway"           "Oman"            
    ## [52] "Pakistan"         "Panama"           "Paraguay"        
    ## [55] "Peru"             "Philippines"      "Poland"          
    ## [58] "Portugal"         "Moldava. Rep. Of" "Romania"         
    ## [61] "Seychelles"       "Singapore"        "Slovenia"        
    ## [64] "Southern Africa"  "Spain"            "Sri Lanka"       
    ## [67] "Sweden"           "Switzerland"      "Thailand"        
    ## [70] "Trinidad Tobago"  "Tunisia"          "Turkey"          
    ## [73] "United Kingdom"   "United States"    "Uruguay"         
    ## [76] "Venezuela"

On obtient un nouveau graphe qui compte 76 sommets au lieu de 80.  

    # aggréger les liens multiples résultant de la réduction du graphe
    g<-simplify(g, remove.multiple = TRUE, remove.loops = TRUE, 
                edge.attr.comb=c(weight="sum", type="ignore"))
                
    g

    ## IGRAPH c3f61b4 DNW- 76 980 -- 
    ## + attr: name (v/c), Country (v/c), coordx (v/n), coordy (v/n),
    ## | wdsystem1980 (v/n), wdsystem1994 (v/n), continent (v/n), xcoord2
    ## | (v/n), ycoord2 (v/n), GDP1995 (v/n), label (v/c), weight (e/n)
    ## + edges from c3f61b4 (vertex names):
    ##  [1] 2->9  2->10 2->12 2->57 2->79 3->6  3->12 3->22 3->34 3->35 3->45
    ## [12] 3->51 3->59 3->66 3->70 4->1  4->3  4->7  4->15 4->17 4->18 4->20
    ## [23] 4->23 4->24 4->26 4->27 4->32 4->33 4->35 4->36 4->37 4->38 4->43
    ## [34] 4->50 4->51 4->53 4->60 4->61 4->62 4->64 4->67 4->68 4->69 4->71
    ## [45] 4->72 4->75 4->76 4->77 5->8  5->74 7->1  7->2  7->4  7->10 7->15
    ## [56] 7->16 7->17 7->18 7->19 7->20 7->23 7->24 7->26 7->27 7->30 7->32
    ## + ... omitted several edges

Suite à cette étape de simplification, le graphe ne compte plus que 980
liens au lieu de 1000 et 76 sommets au lieu de 80. Sans effort
supplémentaire, la visualisation du graphe par défaut n’en est pas
beaucoup plus lisible.  

    # représenter le graphe par défaut
    plot(g)

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-10-1.png" style="display: block; margin: auto;" />

Arguments de la fonction `plot`
-------------------------------

Pour y voir un peu plus clair, commençons par faire varier les
paramètres graphiques. Nous nous appuyons pour cela sur le tutoriel de
Katherine Ognyanova disponible
[*`ici`*](http://kateto.net/network-visualization), et dont la version
2017 a été traduite par Laurent Beauguitte. Cette traduction est
disponible [*`ici`*](https://arshs.hypotheses.org/403).

`igraph` propose de nombreux paramètres pour visualiser les réseaux. Les
options concernant les sommets commencent par `vertex.`, les options
concernant les liens par `edge.`. La liste ci-dessous n’est pas
exhaustive, voir `?igraph.plotting` pour plus d’informations.

**Principaux paramètres graphiques disponibles dans `igraph`**

**Sommets**

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 75%" />
</colgroup>
<tbody>
<tr class="odd">
<td><code>vertex.color</code></td>
<td>Couleur du sommet</td>
</tr>
<tr class="even">
<td><code>vertex.frame.color</code></td>
<td>Couleur de la bordure du sommet</td>
</tr>
<tr class="odd">
<td><code>vertex.shape</code></td>
<td>Choisir entre “none”, “circle”, “square”, “csquare”, “rectangle”</td>
</tr>
<tr class="even">
<td></td>
<td>“crectangle”, “vrectangle”, “pie”, “raster” ou “sphere”</td>
</tr>
<tr class="odd">
<td><code>vertex.size</code></td>
<td>Taille du sommet (15 par défaut)</td>
</tr>
<tr class="even">
<td><code>vertex.size2</code></td>
<td>Taille 2 du sommet (pour un rectangle par exemple)</td>
</tr>
<tr class="odd">
<td><code>vertex.label</code></td>
<td>Vecteur de type caractère utilisé pour nommer les sommets</td>
</tr>
<tr class="even">
<td><code>vertex.label.family</code></td>
<td>Police des labels (e.g.“Times”, “Helvetica”)</td>
</tr>
<tr class="odd">
<td><code>vertex.label.font</code></td>
<td>Fonte : 1 normal, 2 gras, 3, italique, 4 italique gras, 5 symbole</td>
</tr>
<tr class="even">
<td><code>vertex.label.cex</code></td>
<td>Taille de la police (facteur multiplicatif, dépendant de l’OS)</td>
</tr>
<tr class="odd">
<td><code>vertex.label.dist</code></td>
<td>Distance entre le label et le sommet</td>
</tr>
<tr class="even">
<td><code>vertex.label.degree</code></td>
<td>Position du label par rapport au sommet, où 0 est à droite,</td>
</tr>
<tr class="odd">
<td></td>
<td>“pi” à gauche, “pi/2” en dessous et “-pi/2” au dessus</td>
</tr>
</tbody>
</table>

**Liens**

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 75%" />
</colgroup>
<tbody>
<tr class="odd">
<td><code>edge.color</code></td>
<td style="text-align: left;">Couleur du lien</td>
</tr>
<tr class="even">
<td><code>edge.width</code></td>
<td style="text-align: left;">Épaisseur du lien, 1 par défaut</td>
</tr>
<tr class="odd">
<td><code>edge.arrow.size</code></td>
<td style="text-align: left;">Taille des fléches, 1 par défaut</td>
</tr>
<tr class="even">
<td><code>edge.arrow.width</code></td>
<td style="text-align: left;">Épaisseur des flèches, 1 par défaut</td>
</tr>
<tr class="odd">
<td><code>edge.lty</code></td>
<td style="text-align: left;">Type de ligne, dont 0 ou “blank”, 1 ou “solid”, 2 ou “dashed”,</td>
</tr>
<tr class="even">
<td></td>
<td style="text-align: left;">3 ou “dotted”, 4 ou “dotdash”, 5 ou “longdash”, 6 ou “twodash”</td>
</tr>
<tr class="odd">
<td><code>edge.label</code></td>
<td style="text-align: left;">Vecteur de type caractère utilisé pour nommer les liens</td>
</tr>
<tr class="even">
<td><code>edge.label.family</code></td>
<td style="text-align: left;">Police des labels (e.g. “Times”, “Helvetica”)</td>
</tr>
<tr class="odd">
<td><code>edge.label.font</code></td>
<td style="text-align: left;">Fonte : 1 normal, 2 gras, 3, italique, 4 italique gras, 5 symbole</td>
</tr>
<tr class="even">
<td><code>edge.label.cex</code></td>
<td style="text-align: left;">Taille de la police pour les labels des liens</td>
</tr>
<tr class="odd">
<td><code>edge.curved</code></td>
<td style="text-align: left;">Courbure des liens, varie de 0 (FALSE) à 1</td>
</tr>
<tr class="even">
<td><code>arrow.mode</code></td>
<td style="text-align: left;">Vecteur indiquant si les liens doivent avoir des flèches :</td>
</tr>
<tr class="odd">
<td></td>
<td style="text-align: left;">0 pas de flèche, 1 avant, 2 arrière, 3 les deux</td>
</tr>
</tbody>
</table>

**Autres**

<table>
<tbody>
<tr class="odd">
<td><code>margin</code></td>
<td style="text-align: left;">Marges autour du graphique, vecteur de longueur 4</td>
</tr>
<tr class="even">
<td><code>frame</code></td>
<td style="text-align: left;">si TRUE, le graphique sera encadré</td>
</tr>
<tr class="odd">
<td><code>main</code></td>
<td style="text-align: left;">Titre du graphique</td>
</tr>
<tr class="even">
<td><code>sub</code></td>
<td style="text-align: left;">Sous-titre du graphique</td>
</tr>
<tr class="odd">
<td><code>asp</code></td>
<td style="text-align: left;">Numérique, the aspect ratio of a plot (y/x).</td>
</tr>
<tr class="even">
<td><code>palette</code></td>
<td style="text-align: left;">Gamme de couleurs à utiliser pour les sommets</td>
</tr>
<tr class="odd">
<td><code>rescale</code></td>
<td style="text-align: left;">Coordonnées à [-1,1]. TRUE par défaut.</td>
</tr>
</tbody>
</table>

Il est possible d’utiliser les options de deux manières. La première est
de les préciser dans la fonction `plot` comme dans les exemples
suivants. Puis, comme on le verra plus loin, la seconde méthode pour
personnaliser les graphiques est d’ajouter les attributs à l’objet
`igraph`.

Commençons par réduire la taille des flèches et des sommets et ne
visualisons pas les labels des sommets (utilisation de NA).  

    plot(g, edge.arrow.size=.4, vertex.size=6, vertex.label=NA)

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-11-1.png" style="display: block; margin: auto;" />

Utilisons les attributs des sommets pour faire varier leur couleur
(variables qualitatives ou quantitatives discrètes) et leur taille
(variables quantitative continues).

Gestion des couleurs
--------------------

Trouver de bonnes combinaisons de couleurs est délicat et les
possibilités de construction de gammes de couleurs dans R restent
limitées. Heureusement, des packages spécialisés sont disponibles.  

    # si RColorBrewer n'est pas installé, exécutez la ligne suivante
    # install.packages('RColorBrewer')
    library('RColorBrewer')

Ce package contient une fonction principale, `brewer.pal`. Pour
l’utiliser, il suffit de sélectionner la gamme voulue et le nombre de
nuances souhaité. Voici un exemple.  

    # trouver une gamme de couleur pour attribuer à chaque pays une couleur
    # selon son continent d'appartenance
    display.brewer.pal(6, "Set1")

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-13-1.png" style="display: block; margin: auto;" />

Il est préférable que les couleurs soient bien distinctes pour ne pas
les confondre d’autant qu’il n’y a pas de hiérarchie entre continents
(cela n’aurait pas de sens de choisir une gradation de couleur pour les
représenter).  
  

    # attribuer des couleurs selon le continent
    colrs<- brewer.pal(6, "Set1")
    plot(g, edge.arrow.size=.4,vertex.size=6, vertex.label=NA, 
         vertex.color=colrs[V(g)$continent])

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-14-1.png" style="display: block; margin: auto;" />

On observe, dans la plupart des cas, que les pays appartenant au même
continent se retrouvent à proximité les uns des autres ce qui suggère
qu’une proportion importante d’échanges a lieu au sein des continents
(mais ne le prouve pas !).

En l’absence de légende, nous ne savons pas à quelle couleur correspond
quel continent. La légende est indispensable à la lecture du graphe.

Dans le jeu de données source, les continents sont numérotés de 1 à 6 (1
- Afrique, 2 - Asie, 3 - Europe, 4 - Amérique du Nord, 5 - Océanie, 6 -
Amérique du Sud). Une table de correspondance est nécessaire pour
générer la légende.  
  

    # créer une table de correspondance identifiant numérique - continent
    labelC<-data.frame(c(1:6), c("Afrique", "Asie", "Europe", "Amérique du Nord", 
                                 "Océanie", "Amérique du Sud"))

    # représenter le graphe avec sa légende
    plot(g, edge.arrow.size=.4,vertex.size=6, vertex.label=NA, 
         vertex.color=colrs[V(g)$continent])

    legend(x=-1.5, y=-1.1, labelC[,2], pch=21,
           col="#777777", pt.bg=colrs, pt.cex=2, cex=.8, bty="n", ncol=2)

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-15-1.png" style="display: block; margin: auto;" />

Faire ressortir des groupes de sommets
--------------------------------------

Même si l’on y voit un peu plus clair, il y a encore beaucoup
d’information à clarifier. En particulier, les liens sont bien trop
nombreux et supperposés, ce qui empêche de dégager une structure. Les
partitions de sommets obtenues en tenant compte de la position des
sommets dans le réseau peuvent nous aider à dégager une structure.

L’argument `mark.groups` de la fonction `plot` permet de faire
ressortir, en les encerclant, les “communautés” du graphe. Petit rappel
: ‘les communautés du graphe’ est une formulation raccourcie de
l’expression ‘les communautés du graphe décelées par tel ou tel
algorithme’. Un billet d’humeur sur l’utilisation abusive de
l’expression “les communautés du graphe” par Laurent Beauguitte est
accessible [*`ici`*](https://arshs.hypotheses.org/1314). Comme tous les
bons logiciels d’analyse de réseaux, `igraph` propose plusieurs
algorithmes de détection de communautés.

Deux possibilités s’offrent à nous : soit nous utilisons l’un de ces
algorithmes pour détecter des groupes de sommets très connectés entre
eux, soit nous nous appuyons sur les informations dont nous disposons
déjà sur la structure (partition centre-périphérie) de ce réseau.  

    # option no 1 : Détection de communautés basée sur la propagation des labels 
    clu <- cluster_label_prop(as.undirected(g))

    # la détection de communautés renvoie un objet de classe "communities" 
    # qu'igraph sait comment représenter
    plot(clu, g)

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-16-1.png" style="display: block; margin: auto;" />

Contrairement au résultat obtenu dans le tutoriel de Katherine
Ognyanova, le résultat obtenu ici est absolument moche et illisible.
Plusieurs raisons expliquent cela.

-   Premièrement, nous n’avons pas fixé les paramètres utilisés
    précedemment pour la représentation (taille et couleur des sommets,
    taille des flèches…). Plutôt que de devoir les resaisir à chaque
    fois dans la fonction `plot`, il est possible d’assigner les
    attributs nécessaires à la représentation à l’objet `igraph`,

-   Deuxièmement, nous n’avons pas encore procédé à un filtrage ne
    retenant que les liens dont la valeur est la plus importante. Ce
    filtrage sera essentiel pour y voir plus clair, en particulier dans
    la partie la plus dense du réseau,

-   Troisièmement, nous n’avons pas paramétré l’allure des groupes de
    sommets,

-   Et enfin, nous n’avons pas ajouté de légende !

Améliorons le rendu pour faire ressortir la structure centre-périphérie
du réseau.  

    # fixer tous les paramètres précedemment utilisés pour faciliter la lisibilité

    # taille des flèches
    E(g)$arrow.size <- .4

    # couleur des liens, le paramètre alpha permet de régler la transparence
    E(g)$color <- adjustcolor("grey80", alpha=.6)

    # taille des sommets
    V(g)$size <- 6

    # couleur des sommets
    V(g)$color <- colrs[V(g)$continent]

    # attribuer NA à l'argument vertex.label permet de les neutraliser
    plot(g, vertex.label=NA)

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-17-1.png" style="display: block; margin: auto;" />

Mettons à présent en évidence des groupes de sommets partageant
certaines propriétés structurelles. Trois catégories sont distinguées
dans la partition “wdsystem94” (1 - *core*, 2 - *semiperiphery*, 3 -
*periphery*).  

    # récupérer les identifiants des sommets appartenant à chaque catégorie
    wds1<-which(V(g)$wdsystem1994 %in% 1)
    wds2<-which(V(g)$wdsystem1994 %in% 2)
    wds3<-which(V(g)$wdsystem1994 %in% 3)

    # représenter le réseaux en paramétrant l'allure des groupes de sommets
    plot(g, vertex.label=NA, edge.color = adjustcolor("grey90", alpha=0.1),
         # indiquer la liste des sommets appartenant à chacun des trois groupes
         mark.groups=list(wds3, wds2, wds1), 
         mark.col= F, # indiquer la couleur qui servira de fond aux patatoïdes
         mark.border="grey30", # indiquer la couleur du contour des patatoïdes
         mark.expand = 15, # indique la taille du contour
         mark.shape=1) # indiquer le degré de lissage du contour des patatoïdes

    # ajouter la légende
    legend(x=-1.5, y=-1.1, labelC[,2], pch=21,
           col="#777777", pt.bg=colrs, pt.cex=2, cex=.8, bty="n", ncol=2)

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-18-1.png" style="display: block; margin: auto;" />

On voit se dessiner les trois groupes, sauf que les limites du groupe
central (sommets appartenant au centre) sont noyées derrière la trop
grande quantité de liens représentés.

Filtrer les liens
-----------------

Voyons ce que nous obtenons en ne retenant que les liens les plus
importants en valeur. Plusieurs solutions existent pour déterminer un
seuil comme l’indique Françoise Bahoken dans un tutoriel disponible
[*`ici`*](http://elementr.hypotheses.org/370).  
Nous en examinons deux.

1.  On applique un critère statistique en deçà duquel les flux ne seront
    pas représentés. Pour choisir ce critère, on effectue un résumé
    statistique, pour choisir par exemple, la valeur du flux moyen
2.  On décide de fixer arbitrairement le nombre de liens à représenter :
    ne retenir par exemple que les 100 premiers flux

**Première solution : application d’un critère statistique**  

    summary(E(g)$weight) # résumé statistique

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##     144    2828   11276   66909   44499 3059235

    # choisir un seuil
    seuil <-mean(E(g)$weight)  

    # supprimer les liens dont la valeur est inférieure au seuil choisi
    gtop <- delete_edges(g, E(g)[weight<seuil])

    plot(gtop)

![](seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-19-1.png)

**Deuxième solution : sélectionner les 100 premiers liens**  

    # choisir le nombre de liens à représenter
    cut <-100  

    # sélectionner la valeur des 100 premiers liens
    topliens<-E(g)$weight[order(as.double(E(g)$weight), decreasing=TRUE)][1:cut]

    seuil<-topliens[[cut]] # fixer le seuil

    # supprimer les liens dont la valeur est inférieure au seuil choisi
    gtop <- delete_edges(g, E(g)[weight<seuil])

    plot(gtop)

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-20-1.png" style="display: block; margin: auto;" />

On constate que de nombreux sommets se retrouvent isolés du fait de
l’application de cette sélection des 100 principaux liens. Concentrons
nous sur les relations restantes en supprimant les isolés.

Représenter les principaux échanges
-----------------------------------

    # mesurer le degré
    V(gtop)$degree <-degree(gtop)

    # ne conserver que les sommets dont le degré est strictement supérieur à zéro
    gtop<-induced.subgraph(gtop,v=V(gtop)$degree>0)

    #gtop<-induced.subgraph(gtop,V(gtop)[[V(gtop)$degree>0]])

    # représente le graphe obtenu
    plot(gtop, vertex.label=NA)
    # ajouter la légende en enlevant l'Afrique qui ne compte plus aucun sommet 
    # dans le nouveau graphe
    legend(x=-1.5, y=-1.1, labelC[2:6,2], pch=21,
           col="#777777", pt.bg=colrs[2:6], pt.cex=2, cex=.8, bty="n", ncol=1)

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-21-1.png" style="display: block; margin: auto;" />

Choisir une spatialisation
--------------------------

Pour l’instant, nous ne nous sommes pas occupés de la spatialisation du
graphe mais il existe un très grand nombre d’algorithmes permettant de
positionner les sommets les uns par rapport aux autres en optimisant
différents critères (éviter les recouvrements, rapprocher les sommets
les plus connectés…).

On peut choisir la spatialisation dans la fonction `plot` ou calculer au
préalable les coordonnées des sommets. Il est notamment possible de
définir soi-même les coordonnées en générant une matrice de coordonnées
x,y de dimension N x 2 pour les N sommets du graphe.  

    # tester le jeu de coordonnées qui a été chargé avec les données sources

    coord_source<-cbind(V(gtop)$xcoord2*10, -V(gtop)$ycoord2*10)

    plot(gtop, vertex.label=NA,  layout=coord_source)

    legend(x=-1.5, y=-1.1, labelC[2:6,2], pch=21,
           col="#777777", pt.bg=colrs[2:6], pt.cex=2, cex=.8, bty="n", ncol=1)

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-22-1.png" style="display: block; margin: auto;" />

Le résultat correspond à la spatialisation géographique du graphe. Par
défault, les sommets sont positionnés sur un repère orthonormé étendu de
-1 à 1 sur l’axe des abscisses et des ordonnées. Le script suivant
permet d’afficher le repère.

    # tester le jeu de coordonnées qui a été chargé avec les données sources

    plot(gtop, vertex.label=NA,  layout=coord_source)

    legend(x=-1.5, y=-1.1, labelC[2:6,2], pch=21,
           col="#777777", pt.bg=colrs[2:6], pt.cex=2, cex=.8, bty="n", ncol=1)

    abline(v=0)
    abline(h=0)
    abline(v=1)
    abline(h=1)
    abline(v=-1)
    abline(h=-1)


    text(x = 0.1, y = 0.1, labels= "0", col="red", font=2, cex = 1.5)
    text(x = 1.1, y = 0.1, labels= "1", col="red", font=2, cex = 1.5)
    text(x = -1.1, y = 0.1, labels= "-1", col="red", font=2, cex = 1.5)

    text(y = 0.1, x = 0.1, labels= "0", col="red", font=2, cex = 1.5)
    text(y = 1.1, x = 0.1, labels= "1", col="red", font=2, cex = 1.5)
    text(y = -1.1, x = 0.1, labels= "-1", col="red", font=2, cex = 1.5)

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-23-1.png" style="display: block; margin: auto;" />

Regardons l’ensemble des autres spatialisations disponibles dans
*igraph* :  

    layouts <- grep("^layout_", ls("package:igraph"), value=TRUE)[-1] 
    # supprimer les spatialisations non adaptées à ce graphe
    layouts <- layouts[!grepl("bipartite|merge|norm|sugiyama|tree", layouts)]

    par(mfrow=c(5,3), mar=c(1,1,1,1))
    for (layout in layouts) {
      print(layout)
      l <- do.call(layout, list(gtop)) 
      plot(gtop, vertex.label=NA, edge.arrow.mode=0, layout=l, main=layout, vertex.size=10) }

![](seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-24-1.png)

**L’algorithme de visualisation DH**

On décide d’opter pour le résultat obtenu avec l’algorithme de
*Simulated Annealing* de Davidson et Harel (DH).

C’est une spatialisations de la famille des spatialisations
*force-based* qui tentent de générer un graphe élégant où les liens sont
de longueur similaire et se croisent aussi peu que possible. Ces
approches modélisent le réseau en un système physique. Les sommets sont
chargés d’électricité et se repoussent lorsqu’ils sont trop proches. Les
liens agissent comme des ressorts qui rapprochent les sommets connectés.
Au bout du compte, les sommets sont disposés régulièrement sur le plan
et les sommets partageant des connexions sont proches les uns des
autres. L’inconvénient de ces algorithmes est qu’ils sont assez lents
donc peu souvent utilisés pour des graphes de plus de ~1000 sommets. Il
est possible de régler le paramètre `weight` qui augmente les forces
d’attraction entre les sommets connectés par des liens plus intenses.

Ces spatialisations ne sont pas déterministes : différentes exécutions
donneront des résultats légèrement différents. Sauvegarder les
coordonnées dans un objet, ici `ldh`, permet de garder la même
représentation graphique (position des sommets), ce qui peut être utile
pour représenter l’évolution d’un graphe ou différents types de
relations entre les sommets.

Fixer la position des sommets selon un algorithme donné
-------------------------------------------------------

    ldh <- layout_with_dh(gtop)
    plot(gtop, vertex.label=NA, layout=ldh)

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-25-1.png" style="display: block; margin: auto;" />

Bien que le filtrage ait permis de retenir les liens les plus importants
et les sommets qui sont impliqués dans les échanges les plus importants,
cette visualisation ne permet pas d’observer de variations entre liens
et entre sommets.

Tâchons de différencier liens et sommets :

-   utilisons la valeur du PIB par habitant des pays pour faire varier
    la taille des sommets
-   utilisons la valeur des échanges commerciaux pour faire varier
    l’épaisseur des liens

Fixer la taille des cercles
---------------------------

Comment faire en sorte que la taille des sommets (surface des cercles)
soit véritablement proportionnelle à la variable souhaitée ? Par défaut,
les cercles sont dimmensionnés en fonction du rayon ce qui fausse la
proportionnalité. Lorsque le rayon d’un cercle est deux fois plus grand
que le rayon d’un autre cercle, la surface de ce cercle est 4 fois plus
grande (N. Yau, 2013, p. 32). Ce que nous voulons c’est que la
proportionnalité porte sur la surface et non sur le rayon.

Puisque par définition le rayon est égal à la racine carré de la surface
du cercle (divisé par pi) et que nous voulons que la surface des cercles
soit proportionnelle au PIB par habitant, nous devons utiliser la racine
carré du PIB par habitant pour dimensionner le rayon des cercles.

Pour contrôler la représentation des variables (taille des cercles et
épaisseurs des liens), il est préférable de fixer la taille maximale. La
taille maximale des cercles est fixée à l’aide d’un paramètre qu’on
appelera ‘*inches*’ (pouces).  

    # fixer le paramètre de taille
    inches<-0.08

    # choisir la variable quantitative à représenter
    var<-V(gtop)$GDP1995

    # fixer la surface du plus grand cercle
    smax <- inches * inches * pi

    # fixer le rayon des cercles
    # on multiplie par 200 car le package igraph divise, par défaut, 
    # la taille des rayons par 200 (version 1.2.4.1)
    siz <- sqrt((var * smax/max(var))/pi)
    size<-siz*200

    # fixer la taille des cercles
    V(gtop)$size<-size

    # visualiser
    plot(gtop, vertex.label=NA, layout=ldh)

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-26-1.png" style="display: block; margin: auto;" />

On ajoute une légende à l’aide des fonctions du package `cartography`.
La fonction `legendCirclesSymbols` permet d’indiquer à quelle valeur
correspond la taille des cercles, et la fonction `legendGradLines`
permet d’indiquer à quelle valeur correspond l’épaisseur des liens. Pour
utiliser ces fonctions, il convient de fixer en amont la taille des
cercles et l’épaisseur des liens qui seront représentés dans la légende.
On les fixe en utilisant la fonction `seq` qui permet de sélectionner
plusieurs valeurs entre une valeur minimale et une valeur maximale. Ce
seront les valeurs affichées dans la légende.

Ajouter une légende indiquant la taille des cercles
---------------------------------------------------

    # sélectionner les valeurs des 4 tailles de cercles qui seront représentés dans la légende
    varvect <- seq(max(var), min(var), length.out = 4)

    # générer une nouvelle représentation
    plot.new()

    # enregistrer les paramètres graphiques d'origine
    opar<-par()

    # modifier l'étendue du graphe en normalisant les coordonnées des sommets
    ln <- norm_coords(ldh, xmin=-1, xmax=1, ymin=-1, ymax=1)

    # modifier les paramètres graphiques

    # 1. créer une marge en haut afin de laisser la place pour le titre 
    # mar est un vecteur numérique qui permet de fixer des marges dans l'ordre suivant :
    # bas, gauche, haut, et droite

    # 2. se placer dans un nouvel espace de coordonnées : 
    # usr permet de fixer l'étendue de la représentation dans l'ordre suivant:
    # xmin, xmax, ymin, et ymax
    # délimiter une étendue plus grande que l'étendue du graphe  
    # cela permet de laisser de la place pour la légende

    par(mar=c(0,0,1.2,0), usr=c(-1.5,1.5,-1.5,1.5)) 

    # représenter le graphe
    plot(gtop, vertex.label=NA, rescale=F, layout=ln, add=T) 

    # ajuster le paramètre de taille des cercles de la légende aux paramètres graphiques
    # pour contrôler l'effet de la fonction xinch appliquée, par défaut, 
    # dans le package cartography (version 2.2.0)
    # cette modification permet de s'assurer que la taille des cercles de la légende
    # correspondra à la taille des cercles affichés dans le graphe
    xinch<-diff(par("usr")[1L:2])/par("pin")[1L]
    sizevect<-inches/xinch

    # ajouter la légende indiquant la taille des cercles
    legendCirclesSymbols(pos =  "topleft", # position de la légende
                    title.txt ="PIB par habitant", # titre
                    title.cex = 0.7, values.cex = 0.6, # taille des caractères
                    var = varvect, # valeurs de la variable à afficher
                    values.rnd = 0, # nombre de chiffres après la virgule
                    inches = sizevect, # taille des cercles à afficher
                    col = "white", # couleur des cercles
                    frame = F,    # cadre autour de la légende
                    style = "e") # style de légende : "e" pour "étendue", "c" pour "compacte"

    # ajouter la légende indiquant la couleur des cercles
    legend(x=-1.4, y=-1, labelC[2:6,2], pch=21,
           col="#777777", pt.bg=colrs[2:6], pt.cex=2, cex=.8, bty="n", ncol=1)

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-27-1.png" style="display: block; margin: auto;" />

Fixer l’épaisseur des liens
---------------------------

    # fixer la largeur du plus gros flux
    maxsize<-10 # epaisseur du plus gros lien

    # paramétrer l'épaisseur des liens
    E(gtop)$width<-((E(gtop)$weight/max(E(gtop)$weight))*maxsize) 

    # fixer les seuils et épaisseurs de liens qui seront représentés dans la légende

    # 1. retenir 5 valeurs entre la val. min et max
    breaks.edge<-seq(max(E(gtop)$weight), min(E(gtop)$weight), length.out = 5) 

    # 2. liste des épaisseurs des liens apparaissant dans la légende
    lwd.edge<-((breaks.edge[1:4]/max(E(gtop)$weight))*maxsize) 

Ajouter une légende indiquant l’épaisseur des liens, un titre, une source, etc.
-------------------------------------------------------------------------------

    # activer la ligne suivante si vous souhaitez une sortie pdf ou svg
    #pdf(file="world_trade.pdf") #svg(file="world_trade.svg")

    # créer une nouvelle représentation
    plot.new()

    # normaliser l'espace dans lequel les sommets sont positionnés 
    ln <- norm_coords(ldh, ymin=-1, ymax=1, xmin=-1, xmax=1)

    # fixer de nouveaux paramètres graphiques
    par(mar=c(0,0,1.2,0), usr=c(-1.8,1.8,-1.8,1.8)) 

    # adapter le paramètre de taille des cercles de la légende
    # en tenant compte des paramètres graphiques sélectionnés
    xinch<-diff(par("usr")[1L:2])/par("pin")[1L]
    sizevect<-inches/xinch

    # représenter le réseau et les labels des sommets
    plot(gtop, rescale=F, layout=ln, add=T, main= "métaux", edge.width=E(gtop)$width,
         vertex.label.family="sans", # police des labels
         vertex.label.degree=-pi/2, # angle de positionnement des labels
         vertex.label.dist=sqrt(V(gtop)$size)/pi +0.2, # distance des labels
         vertex.label.cex=0.5) # taille des labels

    # ajouter la légende indiquant la taille des cercles
    legendCirclesSymbols(pos = "topright", title.txt ="PIB par habitant",
                    title.cex = 0.6, values.cex = 0.5,
                    var = varvect, inches = sizevect, #taille des cercles
                    col = "white", frame = F, 
                    style = "e") # choix du style "étendu" pour plus de lisibilité

    # ajouter la légende indiquant l'épaisseur des liens
    legendGradLines(pos = "topleft", # position de la légende
                   title.txt = "Importations (1000 US$) ", # titre de la légende
                   title.cex = 0.6, values.cex = 0.6, # taille des caractères
                   breaks=breaks.edge, # intervalles des valeurs représentées
                   lwd = lwd.edge, # épaisseur des liens
                   col = "gray80", # couleur des liens
                   values.rnd = 0, # nombre de chiffre après la virgule
                   frame = F) # cadre de la légende

    # ajouter la légende indiquant la couleur des cercles
    legend(x=-1.75, y=-0.9, labelC[2:6,2], pch=21,
           col="#777777", pt.bg=colrs[2:6], pt.cex=1, cex=.6, bty="n", ncol=1)

    # spécifier le titre, la source et l'auteur du graphique
    layoutLayer(title = "Commerce mondial du métal en 1994", coltitle = "white",
    sources = "Source: D.A. Smith and D.R. White, 1992 repris par W. De Nooy, 2001", 
                # possibilité d'ajouter le Nord et l'échelle si nécessaire
                scale = NULL, north=F,
                author = "M. Maisonobe, 2019",  frame = TRUE, col = "#688994")

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-29-1.png" style="display: block; margin: auto;" />

    # activer la ligne suivante si vous souhaitez nettoyer la fenêtre graphique
    #dev.off()

Produire une figure par continent à l’aide d’une boucle `for`
-------------------------------------------------------------

L’intérêt de fixer les paramètres graphiques, y compris les paramètres
nécessaires à la production d’une légende, est qu’il est ensuite
possible d’automatiser la production d’une série de figures. La légende
s’ajutera en fonction des valeurs maximales (taille maximale des
cercles, épaisseur maximale des liens). Faisons l’expérience avec le
graphe du commerce international du métal. Déduisons de ce graphe
mondial, un graphe par continent en utilisant l’attribut `continents` et
représentons les 6 graphes ainsi obtenus à l’aide d’une boucle `for`.  

    #on revient aux paramètres graphiques d'origine

    par(opar)

    for ( i in 1:6) { 

    gc<- induced.subgraph(g,vids=(which(V(g)$continent==i)))

    # créer une nouvelle représentation
    plot.new()

    # spatialiser et normaliser l'espace dans lequel les sommets sont positionnés 
    lgc <- layout_with_gem(gc)
    lgc <- norm_coords(lgc, ymin=-1, ymax=1, xmin=-1, xmax=1)

    # fixer de nouveaux paramètres graphiques
    par(mar=c(0,0,1.5,0), usr=c(-1.3,1.7,-1.3,1.7)) 

    # couleur des liens, le paramètre alpha permet de régler la transparence
    E(gc)$color <- adjustcolor("grey70", alpha=.7)

    # fixer l'épaisseur des liens
    # ajouter une constante pour s'assurer que les petits liens soient visibles
    E(gc)$width<-((E(gc)$weight/max(E(gc)$weight))*maxsize)+1

    # fixer les seuils et épaisseurs de liens qui seront représentés dans la légende
    # ajouter la même constante que celle ajoutée à E(gc)$width
    breaks.edge<-seq(max(E(gc)$weight), min(E(gc)$weight), length.out = 5) 
    lwd.edge<-((breaks.edge[1:4]/max(E(gc)$weight))*maxsize)+1

    # fixer la taille des cercles

    # 1. taille des cercles

    # fixer la variable à représenter
    var<-V(gc)$GDP1995
    # fixer la taille des rayons
    siz <- sqrt((var * smax/max(var))/pi)
    size<-siz*200
    V(gc)$size<-size

    # sélectionner les valeurs des 4 tailles de cercles qui seront représentés dans la légende
    varvect <- seq(max(var), min(var), length.out = 4)

    # adapter le paramètre de taille des cercles de la légende 
    # en tenant compte des paramètres graphiques sélectionnés
    xinch<-diff(par("usr")[1L:2])/par("pin")[1L]
    sizevect<-inches/xinch

    # lancer la représentation
    plot(gc, rescale=F, layout=lgc, add=T, main= "métaux", edge.width=E(gc)$width,
        vertex.label.family="sans", vertex.label.color="darkblue",
         vertex.label.dist=sqrt(V(gc)$size)/pi +0.7, vertex.label.cex=0.6) 

    # ajouter la légende indiquant la taille des cercles
    legendCirclesSymbols(pos = "topright", title.txt ="PIB par habitant",
                    title.cex = 0.6, values.cex = 0.5,
                    var = varvect, inches = sizevect,
                    col = "white", frame = F, 
                    style = "e") 

    # ajouter la légende indiquant l'épaisseur des liens
    legendGradLines(pos = "topleft", # position de la légende
                   title.txt = "Importations (1000 US$) ", # titre de la légende
                   title.cex = 0.6, values.cex = 0.6, # taille des caractères
                   breaks=breaks.edge, # intervalles des valeurs représentées
                   lwd = lwd.edge, # épaisseur des liens
                   col = "gray80", # couleur des liens
                   values.rnd = 0, # nombre de chiffre après la virgule
                   frame = F) # cadre de la légende


    # spécifier le titre, la source et l'auteur du graphique
    layoutLayer(title = paste("Commerce mondial du métal en", as.character(labelC[i,2]) , 
                              "(1994)"), coltitle = "white",
    sources = "Source: D.A. Smith and D.R. White, 1992 repris par W. De Nooy, 2001", 
                scale = NULL, north=F,
                author = "M. Maisonobe, 2019",  frame = TRUE, col = "#688994")
    }

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-31-1.png" style="display: block; margin: auto;" /><img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-31-2.png" style="display: block; margin: auto;" /><img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-31-3.png" style="display: block; margin: auto;" /><img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-31-4.png" style="display: block; margin: auto;" /><img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-31-5.png" style="display: block; margin: auto;" /><img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-31-6.png" style="display: block; margin: auto;" />

Mettre en évidence des sommets ou des liens spécifiques
-------------------------------------------------------

### 1. Tenir compte de la direction des liens

On peut vouloir colorer les liens selon le sommet émetteur. Le sommet
émetteur pour chaque lien est obtenu avec la fonction `ends()`. Elle
renvoie le sommet d’origine et d’arrivée des liens listés dans le
paramètre `es`. Le paramètre `names` permet d’obtenir soit le nom du
lien soit son identifiant.

    # adapter la couleur du lien au groupe de pays emetteur
    edge.start <- ends(gtop, es=E(gtop), names=F)[,1]
    edge.col <- adjustcolor(V(gtop)$color[edge.start], alpha=.8)

    plot.new()

    # normaliser l'espace dans lequel les sommets sont positionnés 
    ldh<- layout_with_dh(gtop)
    ln <- norm_coords(ldh, ymin=-1, ymax=1, xmin=-1, xmax=1)

    # fixer de nouveaux paramètres graphiques
    par(mar=c(0,0,1.5,0), usr=c(-1.5,1.5,-1.5,1.5)) 

    # adapter le paramètre de taille des cercles de la légende 
    # en tenant compte des paramètres graphiques sélectionnés
    xinch<-diff(par("usr")[1L:2])/par("pin")[1L]
    sizevect<-inches/xinch

    # lancer la représentation
    plot(gtop, rescale=F, layout=ln, add=T, edge.color=edge.col, edge.curved=.1,
         vertex.label=V(gtop)$label, vertex.label.family="sans", 
         vertex.label.dist=sqrt(V(gtop)$size)/pi +1, vertex.label.cex=0.5)  


    # ajouter la légende indiquant la taille des cercles
    legendCirclesSymbols(pos = "bottomright", title.txt ="PIB par habitant",
                    title.cex = 0.6, values.cex = 0.5,
                    var = varvect, inches = sizevect,
                    col = "white", frame = F, 
                    style = "e") 

    # ajouter la légende indiquant l'épaisseur des liens
    legendGradLines(pos = "topleft", # position de la légende
                   title.txt = "Importations (1000 US$) ", # titre de la légende
                   title.cex = 0.6, values.cex = 0.6, # taille des caractères
                   breaks=breaks.edge, # intervalles des valeurs représentées
                   lwd = lwd.edge, # épaisseur des liens
                   col = "gray80", # couleur des liens
                   values.rnd = 0, # nombre de chiffre après la virgule
                   frame = F) # cadre de la légende

    # ajouter la légende indiquant la couleur des cercles
    legend(x=-1.4, y=-0.7, labelC[2:6,2], pch=21,
           col="#777777", pt.bg=colrs[2:6], pt.cex=1, cex=.6, bty="n", ncol=1)

    # spécifier le titre, la source et l'auteur du graphique
    layoutLayer(title = "Commerce mondial du métal en 1994", coltitle = "white",
                sources = "Source: W. De Nooy, 2001", 
                scale = NULL, north=F,
                author = "M. Maisonobe, 2019",  frame = TRUE, col = "#688994")

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-32-1.png" style="display: block; margin: auto;" />

### 2. Représenter la distance à un sommet particulier

Nous désirons parfois centrer la visualisation sur un sommet ou un
ensemble particulier de sommets. Dans notre réseau de commerce, nous
pouvons examiner la diffusion du métal à partir de certains pays. Par
exemple, représentons les distances à partir de la France.

La fonction `distances` renvoie une matrice des plus courts chemins
depuis le(s) sommet(s) listé(s) dans le paramètre `v` jusqu’aux sommets
inclus dans le paramètre `to`.  

    dist.from.FR <- distances(gtop, v=V(gtop)[label=="France Mon."], 
                               to=V(gtop), weights=NA)

    # attribuer des couleurs aux distances
    oranges <- colorRampPalette(c("dark red", "gold"))
    col <- oranges(max(dist.from.FR)+1)
    col <- col[dist.from.FR+1]

    plot(gtop, vertex.color=col, vertex.label=dist.from.FR, edge.arrow.size=.6, 
         vertex.label.color="white", vertex.size=14, 
         sub="Place de la France dans le commerce du métal")

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-33-1.png" style="display: block; margin: auto;" />

On peut également s’intéresser aux voisins d’ordre 1 d’un sommet. La
fonction `neighbors` trouve l’ensemble des voisins d’ordre 1 d’un sommet
donné. Pour trouver les voisins de plusieurs sommets, utiliser la
fonction `adjacent_vertices()`. Pour les voisins d’ordre supérieur à 1,
utiliser la fonction `ego()` avec le paramètre `order`.  

    neigh.nodes <- neighbors(gtop, V(gtop)[label=="France Mon."], mode="out")

    # choisir la couleur pour les voisins d'ordre 1 - degré sortant
    vcol <- rep("gray40", vcount(gtop))
    vcol[V(gtop)$label=="France Mon."] <- "gold"
    vcol[neigh.nodes] <- "#ff9d00"
    plot(gtop, vertex.color=vcol, sub="La France (en jaune) et ses voisins (en orange)", 
         vertex.label=NA)

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-34-1.png" style="display: block; margin: auto;" />

Un moyen d’attirer l’attention sur un groupe de sommets (nous l’avons vu
avec les communautés) est de les entourer.  

    par(mfrow=c(1,2), mar=c(0,0,0,0), pin=c(2,2))

    plot(gtop, mark.groups=c(4,5,8), mark.col="#C5E5E7", mark.border=NA, vertex.label=NA)

    # entourer différents ensembles
    plot(gtop, mark.groups=list(c(4,5,8), c(15:16)), 
              mark.col=c("#C5E5E7","#ECD89A"), mark.border=NA, layout=l, vertex.label=NA)

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-35-1.png" style="display: block; margin: auto;" />

### 3. Représenter un chemin dans le réseau

Il est également possible de mettre en évidence un chemin dans le réseau
:  

    news.path <- shortest_paths(gtop, 
                                from = V(gtop)[label=="France Mon."], 
                                 to  = V(gtop)[label=="Mexico"],
                                 output = "both") # both pour surligner les sommets 
                                                  # et les liens du chemin
    # générer une couleur de liens pour souligner le chemin
    ecol <- rep("gray80", ecount(gtop))
    ecol[unlist(news.path$epath)] <- "orange"

    # générer une épaisseur de liens pour le chemin
    ew <- rep(2, ecount(gtop))
    ew[unlist(news.path$epath)] <- 4

    # générer une couleur de sommet pour le chemin
    vcol <- rep("gray40", vcount(gtop))
    vcol[unlist(news.path$vpath)] <- "gold"

    plot(gtop, vertex.color=vcol, edge.color=ecol, 
         edge.width=ew, edge.arrow.mode=0,sub="De la France au Mexique", vertex.label=NA)

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-36-1.png" style="display: block; margin: auto;" />

On peut mettre en évidence les liens entrants et/ou sortants d’un
sommet, par exemple la France. Pour un seul sommet, utiliser la fonction
`incident()` ; pour plusieurs sommets, utiliser la fonction
`incident_edges()`.  

    inc.edges <- incident(gtop,  V(gtop)[label=="France Mon."], mode="all")

    # choisir la couleur pour les liens sélectionnés
    ecol <- rep("gray80", ecount(gtop))
    ecol[inc.edges] <- "orange"
    vcol <- rep("grey40", vcount(gtop))
    vcol[V(gtop)$label=="France Mon."] <- "gold"
    plot(gtop, vertex.color=vcol, edge.color=ecol, sub="Liens depuis la France", 
         vertex.label=NA)

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-37-1.png" style="display: block; margin: auto;" />

Positionner les sommets manuellement avec tkplot
------------------------------------------------

R et `igraph` permettent la visualisation interactive des réseaux. Cela
peut servir pour bidouiller légèrement la spatialisation d’un petit
graphe. Après avoir fait les ajustements manuels, on peut récupérer les
coordonnées des sommets et s’en resservir ensuite.  

    library(tkrplot)
    tkid <- tkplot(gtop)         # tkid est l'identifiant de ce que va ouvrir tkplot
    l2 <- tkplot.getcoords(tkid) # récupérer les coordonnées depuis tkplot
    plot(gtop, layout=l2, vertex.label=NA)

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-38-1.png" style="display: block; margin: auto;" />

Autres modes de représentation
------------------------------

Il est utile de rappeller que d’autres modes de représentation des
réseaux existent et que le plat de nouilles n’est pas la seule option.

Voici par exemple la `heatmap` du réseau entre 15 pays.  

    netm <- as_adjacency_matrix(gtop, attr="weight", sparse=FALSE)
    colnames(netm) <- V(gtop)$label
    rownames(netm) <- V(gtop)$label

    palf <- colorRampPalette(c("lightyellow", "dark orange")) 
    heatmap(netm[1:15,15:1], Rowv = NA, Colv = NA, col = palf(225), 
            scale="none", margins=c(15,15))

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-39-1.png" style="display: block; margin: auto;" />

En fonction des propriétés du réseau, des sommets ou des liens qui vous
semblent essentielles, de simples graphiques peuvent être plus parlants
qu’un graphe.  

    # Courbe de la distribution des degrés
    deg.dist <- degree_distribution(gtop, cumulative=TRUE, mode="all")
    plot( x=0:max(degree(gtop)), y=1-deg.dist, pch=19, cex=1.2, col="orange", 
          xlab="Degré", ylab="Fréquence cumulée")

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-40-1.png" style="display: block; margin: auto;" />

Pour représenter les relations entre un petit nombres d’acteurs (moins
de 20 sommets), les *chords* sont assez appréciées car visuellement
attractives. Mais attention à ne pas perdre de vue le message important
que vos données ont à faire passer au profit des jolies couleurs et des
belles formes.  

    #on revient aux paramètres d'origine

    par(opar)

    library("circlize")

    rm(el)

    gc<- induced.subgraph(g,vids=(which(V(g)$continent==1)))

    V(gc)$name<-V(gc)$label
    el<-get.edgelist(gc)

    df<-data.frame(from=el[,1], to=el[,2], value=as.vector(E(gc)$weight),stringsAsFactors=F)

    head(df)

    ##              from         to value
    ## 1           Egypt    Tunisia  2153
    ## 2 Southern Africa Madagascar   330
    ## 3 Southern Africa  Mauritius  6805
    ## 4 Southern Africa Seychelles  2833
    ## 5         Tunisia    Algeria  6283
    ## 6         Tunisia    Morocco  1849

    chordDiagram(df)

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-42-1.png" style="display: block; margin: auto;" />

    circos.clear()

La carte est aussi un moyen de rendre compte des résultats de l’analyse
voir même de représenter certaines relations entre entités spatiales
(attention toutefois car les cartes de flux deviennent très vite
illisibles). Voyons comment représenter le degré pondéré (valeur
échangée par pays) sur une carte à l’aide du package *`cartography`*. Il
s’agit de l’adaptation d’un script R mis en ligne par les développeurs
du package disponible
[ici](https://cran.r-project.org/web/packages/cartography/vignettes/cartography.html#linkflow-map).  

    # représenter le fond de carte
    tilesLayer(EUosm)

    # télécharger les données d'eurostat incluses dans le package cartography
    data("nuts2006")
    # ouvrir la table nuts0 (équivalent pays)
    head(nuts0.df)

    ##      id emp2008 act2008 unemp2008 birth_2008 death_2008 gdppps1999
    ## 1418 AT  4089.9  4252.2     162.3      77752      75083     186761
    ## 1782 BE  4445.9  4779.3     333.4     127205     104587     224031
    ## 1783 BG  3360.7  3560.4     199.7      77712     110523      40238
    ## 274  CH  4228.8  4375.4     146.6      76691      61233     186180
    ## 1894 CY   382.9   397.4      14.5       9205       5194      10684
    ## 1893 CZ  5002.5  5232.3     229.8     119570     104948     127323
    ##      gdppps2008  pop1999  pop2008
    ## 1418     259221  7982461  8318592
    ## 1782     308142 10213752 10666866
    ## 1783      82922  8230371  7640238
    ## 274      273871  7123537  7593494
    ## 1894      19316   682862   789269
    ## 1893     210336 10289621 10381130

    # enregistrer le fichier NUTS0
    write.csv(file="nuts0df.csv", nuts0.df)

    # représenter la limite des pays sur le fond de carte
    plot(nuts0.spdf, border = "grey65", lwd = 2, add = TRUE)

    # extraire le graphe de l'Europe contenu dans notre jeu de données sur le métal
    EU<- induced.subgraph(g,vids=(which(V(g)$continent==3)))

    # extraire dans un tableau les données obtenues en calculant le degré pondéré sortant
    V(EU)$name<-V(EU)$label
    V(EU)$strength<-strength(EU, mode="out")
    OUT_EU<-data.frame(Pays=V(EU)$name, WDegre=as.vector(V(EU)$strength), stringsAsFactors=F)

    # ouvrir le fichier de correpondance ID NUTS - labels
    IDNUTS<-read.csv2(file="NUTS0_EU.csv")

    # ajouter une colonne indiquant lorsqu'il existe le degré pondéré
    IDNUTS[,3]<-OUT_EU[,2][match(as.character(IDNUTS[,2]),as.character(OUT_EU[,1]))]

    propSymbolsLayer(spdf = nuts0.spdf, # SpatialPolygonsDataFrame of the countries
                     df = IDNUTS,  # data frame of the regions
                     var = "V3",  # degré pondéré sortant
                     symbols = "circle", # type of symbol
                     border = "white", # color of the symbols borders
                     lwd = 1.5, # width of the symbols borders
                     legend.pos = "topleft", 
                     legend.title.txt = "Total des exportations (en 1000 US$)")
    # Layout plot
    layoutLayer(title = "Place des pays de l'UE dans le commerce européen du métal en 1994",
                sources = "Data: UN Comtrade, 1994", author = 
    "Map tiles by Stamen Design, under CC BY 3.0. Data by OpenStreetMap, under CC BY SA.",
                scale = NULL, frame = TRUE,
                col = "#688994") # color of the frame

<img src="seance_visualisation_2019_final_version_v2_files/figure-markdown_strict/unnamed-chunk-44-1.png" style="display: block; margin: auto;" />
\# Références

Bahoken, F. (2016) Programmes pour Rstudio(c) annexés, in : Contribution
à la cartographie d’une matrice de flux, Thèse de doctorat en Géographie
– sciences des territoires, Université Paris Diderot (Paris 7), Sorbonne
Paris Cités, UMR 8504 Géographie-Cités, 41 p. URL:
<a href="http://elementr.hypotheses.org/370" class="uri">http://elementr.hypotheses.org/370</a>

Ihaka, R. Suppport de cours: “Statistics 120 - Information
Visualisation”. URL:
<a href="https://www.stat.auckland.ac.nz/~ihaka/120/" class="uri">https://www.stat.auckland.ac.nz/~ihaka/120/</a>
Jégou, L. Supports de cours: sémiologie graphique, analyses de réseaux
et cartographie. URL :
<a href="http://www.geotests.net/cours/" class="uri">http://www.geotests.net/cours/</a>

Giraud, T. & Lambert, N. (2019) Commented Scripts to Build Maps with
cartography. URL:
<a href="https://cran.r-project.org/web/packages/cartography/vignettes/cartography.html" class="uri">https://cran.r-project.org/web/packages/cartography/vignettes/cartography.html</a>

Ognyanova, K. (2019) Network visualization with R.
URL:www.kateto.net/network-visualization (traduit par Beauguitte, L.
(2017) Visualisation de réseaux avec R URL:
<a href="https://arshs.hypotheses.org/403" class="uri">https://arshs.hypotheses.org/403</a>)

Pajntar, B. (2006) Overview of algorithms for graph drawing. URL:
<a href="http://ailab.ijs.si/dunja/SiKDD2006/Papers/Pajntar.pdf" class="uri">http://ailab.ijs.si/dunja/SiKDD2006/Papers/Pajntar.pdf</a>

Yau, N. (2013). Data visualisation: de l’extraction des données à leur
représentation graphique. (X. Guesnu, Trad.). Eyrolles.

Visualisation : inventaire des données et outils disponibles en ligne
=====================================================================

-   [La liste de ressources proposées par Katya
    Ognyanova](http://kateto.net/2016/05/network-datasets/)
-   [La liste de ressources proposées par François
    Briatte](https://github.com/briatte/awesome-network-analysis#datasets)
-   [Galerie de représentations issues de l’ouvrage *Visual Complexity*
    de Manuel Lima](http://www.visualcomplexity.com/vc/)
-   [*Visualizing Historical Networks* par le Centre d’Histoire et
    d’Economie de l’Université
    d’Harvard](https://www.fas.harvard.edu/%7Ehistecon/visualizing/index.html)
-   [Le projet Palladio par le centre d’Humanité Numérique de
    l’Université de Stanford](http://hdlab.stanford.edu/palladio/)
-   [Poster: *The Historic Development of Network Visualization* par
    Jürgen Pfeffer et Lin
    Freeman](http://www.pfeffer.at/data/visposter/)
-   [R-graph gallery. Quelques exemples de visualisations générées avec
    *igraph*](https://www.r-graph-gallery.com/248-igraph-plotting-parameters/)
-   [Le site coscimo.net de visualisation interactive des collaborations
    scientifiques mondiales](http://www.coscimo.net/)
-   [Le prototype netmap alliant graphe et carte
    interactive](https://gitlab.com/ljegou/netmap)
-   [Gflowiz: une web application pour générer des cartes interactives
    de flux](http://vlsstats.ifsttar.fr/gflowiz/)
-   [Les algorithmes de spatialisation en accès libre. Wiki
    ouvert](http://amber-v7.cs.tu-dortmund.de/doku.php/tech:layouter)

Visualisation : tutoriels
=========================

-   [L’incontournable tutoriel de Katya Ognyanova. *Network
    Visualization with R*](http://kateto.net/network-visualization)
-   [Sa traduction en français par Laurent Beauguitte. La partie
    consacrée à la visualisation
    statique](https://f.hypotheses.org/wp-content/blogs.dir/2996/files/2017/02/visualiseR.pdf)
-   [Sa traduction en français par Laurent Beauguitte. La partie
    consacrée à la visualisation
    dynamique](https://f.hypotheses.org/wp-content/blogs.dir/2996/files/2017/02/visualiseR_2.pdf)
-   [Présentation et code de James Curley. *Interactive and Dynamic
    Network Visualization in
    R*](http://curleylab.psych.columbia.edu/netviz/)
-   [Créer des cartes de flux avec une légende sous R à l’aide du
    package *cartography* par Timothée Giraud et Nicolas
    Lambert](https://cran.r-project.org/web/packages/cartography/vignettes/cartography.html#linkflow-map)
-   [Suivre l’évolution du package *cartography* et trouver des exemples
    de code sur le carnet de recherche de *Rgeomatic* par Thimothée
    Giraud](https://rgeomatic.hypotheses.org/category/cartography)
-   [Le manuel en ligne du package *igraph* par Gábor
    Csárdi](https://igraph.org/r/#docs)
-   [Dépôt du code du package *cartography* sur Github. Le code des
    fonctions contrôlant les
    légendes](https://github.com/riatelab/cartography/blob/master/R/utils.R)
-   [Dépôt du code du package *igraph* sur Github. Le code de la
    fonction
    plot.igraph](https://github.com/cran/igraph/blob/master/R/plot.R)
-   [Introduction à la visualisation de données : l’analyse de réseau en
    histoire. Par Martin
    Grandjean](http://www.martingrandjean.ch/introduction-visualisation-de-donnees-analyse-de-reseau-histoire/)
-   [La visualisation de données en sciences humaines est un moyen, pas
    une fin ! Par Martin
    Grandjean](http://www.martingrandjean.ch/archive-reseau-visualisation-donnes-sciences-humaines/)
-   [*GraphViz* et *DiagrammeR*. Petit tuto pour visualiser des
    diagrammes sous
    R](https://blog.rstudio.org/2015/05/01/rstudio-v0-99-preview-graphviz-and-diagrammer/)
-   [Créer une carte de flux avec le package R *ggplot* par Peter
    Prevos](http://r.prevos.net/create-air-travel-route-maps/)
-   [Le projet github du package *ggraph* pour la visualisation de
    réseaux sous R. *Grammar of Graph
    Graphics*](https://github.com/thomasp85/ggraph)
-   [Un point sur les paramètres graphiques : “How big is your graph. An
    R Spreadcheet” par Steve
    Simon](https://www.rstudio.com/wp-content/uploads/2016/10/how-big-is-your-graph.pdf)
-   [Un billet de blog au sujet du paramètre graphique usr pour insérer
    des
    légendes](https://chitchatr.wordpress.com/2012/09/18/parusr-is-my-new-friend-for-inserting-legends-in-plots/)
-   [Une courte introduction à la représentation de graphes avec
    *igraph* par Marilia
    Gaiarsa](https://mariliagaiarsa.weebly.com/uploads/3/8/6/2/38628397/igraphtutorialeng.html)
-   [Un TP sur *igraph* par Laurent Tichit et Alberto Valdeolivas
    Urbelz](http://www.dil.univ-mrs.fr/~tichit/bin/tp2/igraph_tutorial2.html)
-   [Stackoverflow. Forum de
    questions](https://stackoverflow.com/questions/tagged/r)
