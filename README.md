**Séance 10 Principes de visualisation** (Marion Maisonobe) : Principes de sémiologie graphique et exercices, ou comment produire un graphe lisible et utile.

Le projet est composé de :

* D'une liste de liens sur le commerce mondial des métaux
* D'une liste d'attributs sur le commerce mondial des métaux
* Du code R permettant d'analyser ce jeu de données
* D'une table de correspondance entre codes NUTS0 et pays européens
* D'une liste de liens utiles pour la visualisation des réseaux
* D'un document pdf et _markdown_ (md) détaillant l'analyse sous R
* D'un diaporama pdf détaillant tout le contenu du cour
    * Principes de sémiologie graphique
    * Principes de spatialisation d'un graphe
    * L'exemple de la distance entre langues européennes
    * L'exemple des données du commerce mondial
    * Pour aller plus loin avec R

Il est possible d'accéder à la version 2017 de cette séance via la branche [Visualisation-de-reseaux-2017](https://framagit.org/MarionMai/Visualisation-de-reseaux/tree/Visualisation-de-reseaux-2017)