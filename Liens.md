# Visualisation : inventaire des données disponibles en ligne

* [La liste de ressources proposées par Katya Ognyanova](http://kateto.net/2016/05/network-datasets/)
* [La liste de ressources proposées par François Briatte](https://github.com/briatte/awesome-network-analysis#datasets)
* [Galerie de représentations issues de l'ouvrage _Visual Complexity_ de Manuel Lima](http://www.visualcomplexity.com/vc/)
* [_Visualizing Historical Networks_ par le Centre d'Histoire et d'Economie de l'Université d'Harvard](https://www.fas.harvard.edu/%7Ehistecon/visualizing/index.html)
* [Le projet Palladio par le centre d'Humanité Numérique de l'Université de Stanford](http://hdlab.stanford.edu/palladio/)
* [Poster: _The Historic Development of Network Visualization_ par Jürgen Pfeffer et Lin Freeman](http://www.pfeffer.at/data/visposter/)
* [Le site coscimo.net de visualisation interactive des collaborations scientifiques mondiales](http://www.coscimo.net/)

# Visualisation : tutoriels

* [L'incontournable tutoriel de Katya Ognyanova. _Network Visualization with R_](http://kateto.net/network-visualization)
* [Sa traduction en français par Laurent Beauguitte. La partie consacrée à la visualisation statique](https://f.hypotheses.org/wp-content/blogs.dir/2996/files/2017/02/visualiseR.pdf)
* [Sa traduction en français par Laurent Beauguitte. La partie consacrée à la visualisation dynamique](https://f.hypotheses.org/wp-content/blogs.dir/2996/files/2017/02/visualiseR_2.pdf)
* [Présentation et code de James Curley. _Interactive and Dynamic Network Visualization in R_](http://curleylab.psych.columbia.edu/netviz/)
* [_GraphViz_ et _DiagrammeR_. Petit tuto pour visualiser des diagrammes sous R](https://blog.rstudio.org/2015/05/01/rstudio-v0-99-preview-graphviz-and-diagrammer/)
* [Introduction à la visualisation de données : l’analyse de réseau en histoire. Par Martin Grandjean](http://www.martingrandjean.ch/introduction-visualisation-de-donnees-analyse-de-reseau-histoire/)
* [La visualisation de données en sciences humaines est un moyen, pas une fin ! Par Martin Grandjean](http://www.martingrandjean.ch/archive-reseau-visualisation-donnes-sciences-humaines/)
* [Créer des cartes de flux avec une légende sous R à l'aide du package _cartography_ par Timothée Giraud et Nicolas Lambert](https://cran.r-project.org/web/packages/cartography/vignettes/cartography.html#linkflow-map)
* [Créer une carte de flux avec le package R _ggplot_ par Peter Prevos](http://r.prevos.net/create-air-travel-route-maps/)
* [Le projet github du package _ggraph_ pour la visualisation de réseaux sous R. _Grammar of Graph Graphics_](https://github.com/thomasp85/ggraph)
* [_ggnet2_: network visualization with _ggplot2_](https://briatte.github.io/ggnet/)

# L'exemple de la distance entre langues européennes

* [_Lexical Distance Among the Languages of Europe_ par Teresa Elms en 2008](https://elms.wordpress.com/2008/03/04/lexical-distance-among-languages-of-europe/)
* [_The theory of lexical distance among European languages by Kostiantyn Tyshchenko_ par Carlo Marino en 2014](http://wantedinlinguistics.blog.tiscali.it/2014/01/14/the-theory-of-lexical-distance-among-european-languages-by-kostiantyn-tyshchenko/)
* [_How much does language change when it travels?_ par Stephan F. Steinbach en 2015](https://alternativetransport.wordpress.com/2015/05/04/how-much-does-language-change-when-it-travels/)
* [_Lexical Distance Among Languages of Europe 2015_ par Stephan F. Steinbach en 2015](https://alternativetransport.wordpress.com/2015/05/05/34/comment-page-1/#comment-483)
* [_How to and the trouble with mapping lexical distance in 2D_ par Stephan F. Steinbach en 2015](https://alternativetransport.wordpress.com/2015/05/20/how-to-and-the-trouble-with-mapping-lexical-distance-in-2d/)
* [_Lexical Distance Diagram in 3D?_ par Stephan F. Steinbach en 2015](https://alternativetransport.wordpress.com/2015/05/24/lexical-distance-diagram-in-3d/)
* [_23 Languages of the World_ par Stephan F. Steinbach en 2015](https://alternativetransport.wordpress.com/2015/06/02/23-languages-of-the-world/)
* [_A Yiddish fleet? What is the definition of 1 Lexical Distance?_ par Stephan F. Steinbach en 2015](https://alternativetransport.wordpress.com/2015/08/12/a-yiddish-fleet-what-is-the-definition-of-1-lexical-distance/)
* [_The "Other Languages" in Ethnologue & Glottolog- isolates, contact languages, sign languages etc_ par Hedvig Skirgård en 2015](http://humans-who-read-grammars.blogspot.fr/2015/11/the-other-languages-in-ethnologue.html)
* [Commentaires sur _Lexical Distance Among the Languages of Europe_ en 2016](http://languagehat.com/lexical-distance-among-the-languages-of-europe/)
* [_Quantitative comparative linguistics_ par Vincent Beaufils en 2016](http://www.elinguistics.net/)
* [The database of the Automated Similarity Judgment Program (ASJP) par Søren Wichmann, Eric W. Holman et Cecil H. Brown en 2016 (version 17)](http://asjp.clld.org/)
* [_Lexical Distance Among the Languages of Europe: a hoax, a political pamphlet or actual popular science?_ par Boban Arsenijevic en 2017](http://inform-al.blogspot.fr/2017/03/lexical-distance-among-languages-of.html)
* [_Lexical Distance, a hoax?_ par Stephan F. Steinbach en 2017](https://alternativetransport.wordpress.com/2017/03/08/lexical-distance-a-hoax/)
* [_Lists of languages_ par Stephan F. Steinbach en 2017](https://alternativetransport.wordpress.com/2017/05/02/lists-of-languages/)
* [_World map of language families from Glottolog_ par Hedvig Skirgård en 2017](http://humans-who-read-grammars.blogspot.fr/)
* [_Database of Swadesh lists_](https://linguistics.stackexchange.com/questions/11037/database-of-swadesh-lists/)